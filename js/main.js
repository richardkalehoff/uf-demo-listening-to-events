var colorPicker = document.getElementById("colorPicker");
var sizePicker = document.getElementById("sizePicker");

colorPicker.addEventListener("change", colorChange);
sizePicker.addEventListener("change", sizeChange);

function colorChange() {
    var hex = "#" + colorPicker.options[colorPicker.selectedIndex].value;
    console.log(hex);
    document.body.style.backgroundColor = hex;
}

function sizeChange() {
    var pxs = sizePicker.options[sizePicker.selectedIndex].value;
    console.log(sizePicker);
    document.body.style.fontSize = pxs;
}
